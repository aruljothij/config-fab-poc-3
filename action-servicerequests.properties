# -------------------------------------------------------------------------------------------
# NOTE : 
# 	This should be used only for DEV instance
# 	For Live instance, all variables should be defined as Environment variables.
# 	Environment variables hold precedence against what is defined in this file
# -------------------------------------------------------------------------------------------

# Allow local configuration to override Remote Externalized configuration
spring.cloud.config.allowOverride=true
# But, only System properties or Env variables (and not local config files) will override externalized configuration
spring.cloud.config.overrideSystemProperties=false



#############################################################################################
###### Global App Specific Configuration
#############################################################################################
# Logging level (DEBUG,INFO,WARN,FATAL)
logging.level.ROOT=INFO
logging.level.com.igtb.api.action.commons=ERROR
# to ensure errors logged by Camel RabbitMQConsumer only if severity is ERROR (it helps avoiding lot of noise at WARNING level)
logging.level.org.apache.camel.component.rabbitmq.RabbitMQConsumer=ERROR

# Used by Spring Boot (Application Context)
server.contextPath=/igtb-servicerequests

# Port 51002
server.port=51002

# Module Abbreviation Name (<= 30 characters and only Upper Case Alphabets A-Z)
# e.g. LMS (for Liquidity), IPSH (for Payments), CNR (for Collections & Receivables)...
# This is used for deriving:
#     - Redis Key name as action-requests:<moduleAbbr>:${channelSeqId} (e.g. action-requests:lms:10581856258-94299189, action-requests:ipsh:..., action-requests:cnr:....)
ModuleAbbr=SRQ

# Header key and value, to be used as security token while calling APIs for this module
iGTBD-AtomicAPI-SharedKey=I4qwGynNIt5DP5zUjjemHR1mEj8Ii6jq

# Redis Database connection details
Redis.DB.Url=localhost:6379
Redis.DB.Password=
# if SSL set to true, Redis client will use standard JRE truststore for SSL connectivity
Redis.DB.SSL.Enabled=false
# "true" indicates hostname mistmatch exception should be avoided (test env specific)
#Redis.DB.SSL.DisableHostNameVerification=false
## For Sentinel connection
#Redis.Sentinel.Enabled=true
#Redis.Sentinel.Urls=52.30.51.155:26379,34.242.88.42:26379,34.252.191.116:26379
#Redis.DB.Master.Name=redis-master
#Redis.DB.Password=redis-password
#Redis.DB.Instance=0
#Redis.Sentinel.FailoverWaitTimeMS=10000

# Prefix to be used while naming keys in Redis (useful when same Redis instance to be used across test environments)
# NOT intended for live env
# NOTE - if this is used, it should be configured in common remote application.properties, so it is applied across modules.
#Redis.KeyPrefix=staging:

# To Enable/Disable DevMsg field in JSON response returned from API
# Y - enables DevMsg in output json response (may be useful for dev env)
# N - disables the same (recommended for production env) (this is default, if not specified)
EnableDevMsgInResponse=N

# Camel Messages to be logged at this level (Possible values: ERROR, WARN, INFO, DEBUG, TRACE, OFF)
CamelMessageLoggingLevel=DEBUG

# Release Batch Size
# It is max number of requests to be released per batch (Approved, Retry batch)
ReleaseBatchSize=200

# Max length of the channelSeqId to be considered while generating a new one.
# minimum supported value for this parameter is 12. default is 20.
# NOTE - if this is used, it should be configured in common remote application.properties, so it is applied across modules.
#StateMgmt.maxChannelSeqIdLength=20

# Represents DataCenter region, country the process events/data belongs to
DataCenter.Region=
DataCenter.Country=

# Represents comma separated multiple service key & function regex patterns, this module is supposed to process events/data related to
# Format :
#   product/subproduct:function, product/subproduct:function, ....
# Example :
#   lqdy/swp.*:.*, lqdy/exec:add
#       indicates all requests/events with
#           - all serviceKeys matching "lqdy/swp.*" format AND related functions matching ".*" format
#           OR
#           - all serviceKeys matching "lqdy/exec" AND related function matching "add"
#       will be accepted for processing.
Digital.ServiceKey.Patterns=account/.*:.*
#Digital.ServiceKey.Patterns=account/servicerequest:add




##################################################################
### Zipkin specific options
##################################################################
spring.zipkin.enabled=false
spring.sleuth.enabled=false
#spring.sleuth.web.additionalSkipPattern=.*/getDetails|.*/updateDetails
#spring.sleuth.sampler.probability=1.0
#spring.zipkin.baseUrl=http://localhost:9411





##################################################################
### Message Broker configuration
##################################################################
# Message Broker which is to be used by Action API Commons for handling triggers/events
# Supported values - RabbitMQ only
Digital.MsgBroker.Type=RabbitMQ
# This is used by Commons for connecting to RabbitMQ Server
Digital.RabbitMQ.Host=localhost
Digital.RabbitMQ.Port=5672
Digital.RabbitMQ.User=guest
Digital.RabbitMQ.Password=guest
Digital.RabbitMQ.VHost=/
# if SSL set to true, RMQ client will use standard JRE truststore for SSL connectivity.
# internally creates rmqTrustManager bean as default TrustManager for Camel producer/consumer endpoints
Digital.RabbitMQ.SSL.Enabled=false

# Used as sleep time (in milliseconds) between each message retry, when msg is requeue'd.
# Keep it commented, unless need to override default value.
#MsgRetrySleepTime=250

# Used as max time (in seconds) a message to be retried for in requeue mode
# Keep it commented, unless need to override default value.
#MsgRetryMaxTime=86400





#############################################################################################
###### Release Retry Configuration
#############################################################################################
RelRetryCfgIdentifierKeys=SERVICEREQUEST_ANY

###
# Release Retry Config Identifier key based configuration
# 
# RelRetryCfgIdentifier.<identifierKey>.payloadType=
# RelRetryCfgIdentifier.<identifierKey>.requestType=
# RelRetryCfgIdentifier.<identifierKey>.maxReleaseRetry=
# Where,
#	<payloadType,requestType> together forms a unique key combination for providing related configuration 
#	payloadType = type of the payload e.g. SweepStructure, LoanAgreement or any
#	requestType = type of the request e.g. create, update, delete or any
# 	maxReleaseRetry = to indicate how many times this message should be retried for releasing before it is marked as Failed
#			(field is optional - default value will be applied)
#
# Configuration is applied in following order of priority, for a combination of <payloadType> and <requestType>:
# 1. <payloadType> and <requestType> exactly match with values provided here.
# 2. Else - <payloadType> and "any"
# 3. Else - "any" and <requestType>
# 4. Else - "any" and "any"
# 5. Else - default retry configuration is applied
#
###
RelRetryCfgIdentifier.SERVICEREQUEST_ANY.payloadType=ServiceRequest
RelRetryCfgIdentifier.SERVICEREQUEST_ANY.requestType=any
RelRetryCfgIdentifier.SERVICEREQUEST_ANY.maxReleaseRetry=5

# "update" example
#RelRetryCfgIdentifier.SERVICEREQUEST_UPDATE.payloadType=ServiceRequest
#RelRetryCfgIdentifier.SERVICEREQUEST_UPDATE.requestType=update
#RelRetryCfgIdentifier.SERVICEREQUEST_UPDATE.maxReleaseRetry=5


##################################################################
### Release Connector Specific Configuration
##################################################################
RelConnector.JMS_CONNECTOR.route=direct:JmsConnectorRoute

#Backend.ReleaseServiceUri=http://localhost:51003
### Transformer routes
# ServiceRequest specific
RelConnector.SERVICEREQUEST_TXFMR.route=direct:TransformReleaseServiceRequestRoute


### Backend Endpoints
# ServiceRequest specific
# Endpoint using JMS custom-jms:queue
#RelConnector.SERVICEREQUEST_MUTATION.endpoint=activemq:queue:service_req_initiate?disableReplyTo=true
#RelConnector.SERVICEREQUEST_MUTATION.endpoint=direct:BackendReleaseStubRoute

### Routing Slips
# ServiceRequest specific
RelConnector.ServiceRequestCreate.routingSlip=${RelConnector.SERVICEREQUEST_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.SERVICEREQUEST_MUTATION.endpoint}
		
RelConnector.ServiceRequestAny.routingSlip=${RelConnector.SERVICEREQUEST_TXFMR.route},\
		${RelConnector.JMS_CONNECTOR.route},\
		${RelConnector.SERVICEREQUEST_MUTATION.endpoint}

##################################################################
### Validation And Release specific Connector routes
##################################################################
ValConnector.HTTP_CONNECTOR.route=direct:HttpConnectorRoute
#RelConnector.HTTP_CONNECTOR.route=direct:HttpReleaseConnectorRoute

### Transformer routes
# ServiceRequest specific
ValConnector.SERVICEREQUEST_TXFMR.route=direct:TransformServiceRequestRoute

### Backend Endpoints
# ServiceRequest specific
# Validation Endpoint using Interface approach
#ValConnector.SERVICEREQUEST_HTTP.endpoint=jetty://http://localhost:51003/stubs/validators/v1/user
ValConnector.SERVICEREQUEST_HTTP.endpoint=bean:validateServiceRequestBean?method=validateResourcePayload


### Routing Slips
# ServiceRequest specific
ValConnector.ServiceRequestCreate.routingSlip=${ValConnector.SERVICEREQUEST_TXFMR.route},\
		${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnector.SERVICEREQUEST_HTTP.endpoint}
ValConnector.ServiceRequestAny.routingSlip=${ValConnector.SERVICEREQUEST_TXFMR.route},\
		${ValConnector.HTTP_CONNECTOR.route},\
		${ValConnector.SERVICEREQUEST_HTTP.endpoint}

Digital.JWT.ExpiryCheck.Flag=false


#################################################################################################
### State Management controls for draft and initiate states for unhappy validation scenarios
#################################################################################################
# Flag to indicate if initiate request should be marked as failed (500) if validate call gets timed out. Defaults to false. 
StateMgmt.failInitiateOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if validate call gets timed out during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValTimeout=true
# Flag to indicate if original draft should be retained or not, if there are validation failure (400,422) during draft-2-initiate transition. Defaults to false. 
StateMgmt.retainDraftOnValFailure=true
#################################################################################################




#############################################################################################
###### Release Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to Release Batch Trigger events 
RelTriggerHandler.CamelComponent=rabbitmq
RelTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# PayloadTypes - comma separated list of Resources, for which this module need to support running Release Batches
RelTriggerHandler.PayloadTypes=ServiceRequest



#################################################################################################
###### State Update Handler specific Configuration
#################################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for subscribing/publishing to B/E State Update events 
StateUpdHandler.CamelComponent=rabbitmq
StateUpdHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30



#################################################################################################
###### Event Publish Handler specific Configuration
#################################################################################################
# Name of the Apache Camel Component,
# which is to be used by Commons package for publishing Events / Triggers
MsgPubHandler.CamelComponent=rabbitmq
#MsgPubHandler.CamelComponent.Options=



#############################################################################################
###### Purge Trigger Handler specific Configuration
#############################################################################################
# Name of the Apache Camel Component (which is configured at Domain level),
# which is to be used by Commons package for subscribing/publishing to Purge Batch Trigger events 
PurgeTriggerHandler.CamelComponent=rabbitmq
PurgeTriggerHandler.CamelComponent.Options=prefetchEnabled=true&prefetchCount=30

# Minimum number of days the data must be retained in State Store (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.MinRetentionDays=720
# Flag indicating whether purging of IN_PROGRESS requests from State Store is allowed or not (irrespective of what is specified in PurgeStateStoreTrigger)
PurgeTriggerHandler.PurgeOfInProgressAllowed=false


# Comma separated list of Payload Types, for which this module need to support running Purge Batches
PurgeTriggerHandler.PayloadTypes=ServiceRequest





#############################################################################################
###### Registering mgmt endpoints to Eureka
#############################################################################################
eureka.instance.statusPageUrlPath=${server.contextPath}${management.context-path}/info
eureka.instance.healthCheckUrlPath=${server.contextPath}${management.context-path}/health





#################################################################################################
### iGTB Certificates and PrivateKeys related configuration (local default)
#################################################################################################
# "kid" of the iGTB default certificate that is to be used for signing/verification within platform
igtb.default.privateKeyId = igtb-default-v1
# key name prefix used by SDK to load all certificates and privateKeys
# with below configuration, all "igtbCertificates.*" and "igtbPrivateKeys.*" properties will be used by SDK
igtb.default.certificates.prefix = igtbCertificates.
igtb.default.privateKeys.prefix = igtbPrivateKeys.
# default certificates in format <certificate-prefix><kid> = <actual-certificate>
#igtbCertificates.library-default-v1 = -----BEGIN CERTIFICATE-----MIIBDzCBuqADAgECAgRaMZAeMA0GCSqGSIb3DQEBCwUAMA8xDTALBgNVBAoMBGlndGIwHhcNMTcxMjEzMjAzOTU4WhcNMjcxMjEzMjAzOTU4WjAPMQ0wCwYDVQQKDARpZ3RiMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBALWYSsXPFjWAu1vb/6bo1Xd3+fOOQoyLQ2r4fZlwKCLFuToYgA9tvb+/egZBHf2eMhYOLuKni49eF0zeBRSA/mcCAwEAATANBgkqhkiG9w0BAQsFAANBADh+bEQb0V821KIc4svhj6rZnbklrsEdi2jlcfkvUHbKohUILhA3JNXfJ7Ss6FJDznfVbnQ09+SLyZ/3KyVeQU4=-----END CERTIFICATE-----
#igtbCertificates.entl = -----BEGIN CERTIFICATE-----MIIBtjCCAWCgAwIBAgIEWjZoBTANBgkqhkiG9w0BAQsFADBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwHhcNMTcxMjE3MTI1MDEzWhcNMTgxMjE3MTI1MDEzWjBiMQswCQYDVQQGEwJJTjELMAkGA1UECAwCVE4xDDAKBgNVBAcMA0NITjESMBAGA1UECgwJSW50ZWxsZWN0MRUwEwYDVQQLDAxpZ3RiLWNieC5jb20xDTALBgNVBAMMBGVudGwwXDANBgkqhkiG9w0BAQEFAANLADBIAkEAq8xGOS0xnV+2+9ifAT3Ve2DHwyGID4JW7FNyh/7IWFQXlG9lqfHvbasGVlrKQNFO3SKqa/4tFziYWd5P0J9YnQIDAQABMA0GCSqGSIb3DQEBCwUAA0EAPPiYIpBxDH3hgewqhv8wvB5QziHvve9aOOl7IogFV82Ypcis/8pgtu16hsclJrC927Cf6r5ugVfKKVsspoRXig==-----END CERTIFICATE-----
#igtbCertificates.NDFBOTFCQjA3RDMyNDhCM0IyREY3RTJGN0IwMkU4RTk5RDlDODEzNw = -----BEGIN CERTIFICATE-----MIIC/zCCAeegAwIBAgIJOqpL6Sw/5g0pMA0GCSqGSIb3DQEBCwUAMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTAeFw0xNzEyMDUwNjEwMDlaFw0zMTA4MTQwNjEwMDlaMB0xGzAZBgNVBAMTEmlndGItY2J4LmF1dGgwLmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMhNIs8y/ylb25wdtyTub0IaagKUbMiLvOeQrXqf5PN0fTSzDItRHbqlWUDv+U4ajzJhND9zyDBefY3ZCxcvC6gIR4blixDtoebjoLW3GxgIdM5DOWxgRFinbZdkpCUgHdvjlXgsPPHQttiTqqRhPFJjHHKaLepqMsD/TAO0KOBLy/UXncKlp1lD6j9ASM+1nycdvitDW31zaUvPFWTFQjnUF84Y9yqhtOJtOHOiZGE2Vz4wLRbGrNYqTcTMZf+R6TqlqgmqOCx9eblqJCjOMfYo+tU0uyLW+1CIexFOT9IUum1KnCZQvMOOrYWbAqb1HCtUAi8s4toYk2T/P+zfJwsCAwEAAaNCMEAwDwYDVR0TAQH/BAUwAwEB/zAdBgNVHQ4EFgQUITR+sINi+t7sX4aYeJhQJRjNjO8wDgYDVR0PAQH/BAQDAgKEMA0GCSqGSIb3DQEBCwUAA4IBAQCO6L0TLFrH2QaUtc0cKrLFPwGhqLmIoyP3cYOjjT0ytASEssWzjPJobGNNC5AuRo96tNeC+t4Keh6YhcBrUbuZXQDa2OCM75xY4D/lY3VyXBDAicMZhHLc/urD+LTyWcIljFN6ZIYEgnwkpADzVDSiju+gCn3mVoDvidl8Bk2xNi411GDh20CeF49bz0mrYxXxwKqVtEx6LK2Mam/+4hKR7bgrrReeF06P1XNwMO/YZsZPE2jori7UdhhvDOWivdE2kbGGuvRh7IzY7hs7lOyE57T4z35O29JLABF0E9k/CER2YjvfrDwNf6ttUUIyU/T3AR4I7LuMSMQ2oVa3OzUu-----END CERTIFICATE-----
#igtbCertificates.igtb-default-v1 = -----BEGIN CERTIFICATE-----MIIBDzCBuqADAgECAgRcDiAMMA0GCSqGSIb3DQEBCwUAMA8xDTALBgNVBAoMBGlndGIwHhcNMTgxMjEwMDgxMzAwWhcNMTkxMjEwMDgxMzAwWjAPMQ0wCwYDVQQKDARpZ3RiMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMe5Wm/yyvpHA3z3UYjYYexUnw5vhSXbqDBvgqdbWUbnyikXZMn7AkGL2zgy2ltKFO5ltJQWmEo+QLHqwQbdytcCAwEAATANBgkqhkiG9w0BAQsFAANBAHkc46x/WHuVNAojEGEZE7OlOsLAcj2D+nur63R8kt1c72Tixot+LcV044RxSI0qZaT1hoTiikOSoSJ0sJloJrY=-----END CERTIFICATE-----
# default private keys in format <privateKey-prefix><kid> = <actual-privateKey>
#igtbPrivateKeys.library-default-v1 = -----BEGIN PRIVATE KEY-----MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEAtZhKxc8WNYC7W9v/pujVd3f5845CjItDavh9mXAoIsW5OhiAD229v796BkEd/Z4yFg4u4qeLj14XTN4FFID+ZwIDAQABAkAAp/8RLZk8x+0lZ6tKAkpkhvb3cIVWC691L1ZeMRURYcL6kh2mfJX336/Bx61KG0sOQhlhLy96qNEj7TWB8RFlAiEA33mhWvM0JeS0b35uFWeaHKryX+u8oCxljYFnkca0XvMCIQDQBkNhti6nCEZZ1bkEIs7DNlYHlkjImq3PbnxJWFjHvQIgPga2YFYMbVATPLbsuwgsYDSaXUFC83ofHa2DH5T1Tz0CIQCGSSTCNNrU1JCleUPgGaAjgDX029zMMp204/uzVSaqeQIgF4rWschznVKJT8TqZrPgVcL9/xroE8q6Kg+F1CPlCu4=-----END PRIVATE KEY-----
#igtbPrivateKeys.igtb-default-v1 = -----BEGIN PRIVATE KEY-----MIIBVQIBADANBgkqhkiG9w0BAQEFAASCAT8wggE7AgEAAkEAx7lab/LK+kcDfPdRiNhh7FSfDm+FJduoMG+Cp1tZRufKKRdkyfsCQYvbODLaW0oU7mW0lBaYSj5AserBBt3K1wIDAQABAkBPmDjIgJQP5hknxlCCHun3i0nQMPV5Pu7qTqZdF/SeYrEFn4Hr9Kes6Fgsu3MR1jmb9nARanGqPI97cITjnTCxAiEA8IgqqQvWK5nZLkJaCQWW3FZIH9xfu5gBiQM8yroe7hsCIQDUkV/FLV4afKYZakFYO3pMZEirND/R7WXZGyURmx5x9QIhAJovDvmRxtXoJtmKWuCRnqTbAhy6TPuZctJ7KsrhB9alAiA59Fi0HaZmek2e6EVglPbHFSBq7RfpWFxIUQpOPoiScQIhAJiJX6OUBg+77mLdYCSVQQ1MzAcgVmWP1UX/JrFGLa4E-----END PRIVATE KEY-----
#################################################################################################






#############################################################################################
### Quest specific parameters 
#############################################################################################
### Feign target client name
### IF "digital-gatekeeper", then service will be resolved via Eureka service discovery (by default)
### IF "quest-direct", then service will be connected directly using URIs (in quest-direct.ribbon.listOfServers)
digital.quest.feignClientName=digital-gatekeeper
        
### Quest URI (IF using Gkp then "/quest/graphql". IF using Quest directly then "/graphql")
digital.quest.uri=/quest/graphql

### To provide details, when connecting with Quest using direct Host/IP (non-Eureka approach)
quest.url=http://digital-quest.cfapps.io
quest-direct.ribbon.listOfServers=${quest.url}
quest-direct.ribbon.eureka.enabled=false

ribbon.ConnectTimeout=20000
ribbon.ReadTimeout=20000
#############################################################################################






#############################################################################################
### Exception handling specific log configuration 
#############################################################################################
### Log level-step-up configuration
### Keep it commented, unless there is need to customize
#logging.levelStepUp.CBXS_SYS_BE_REL_RETRY=WARN:0,ERROR:120
#logging.levelStepUp.CBXS_ACT_COM_BE_EVT_REQUEUE=INFO:0,WARN:5,ERROR:120

### DLQ specific action_code's
### Keep it commented, unless there is need to customize
#logging.dlqActionCode.CBXS_SYS_MSG_INVALID=INV_MSG
#logging.dlqActionCode.CBXS_SYS_RETRY_EXCEEDED=OFFLINE
#############################################################################################

####ACTIVEMQ CONFIGURATIONS START########

Backend.activemqUrl=tcp://10.197.12.8:61616
Backend.ReleaseServiceUri=tcp://10.197.12.8:61616?jms.userName=admin&jms.password=admin;
Backend.JMS.InitialContextFactory=org.apache.activemq.jndi.ActiveMQInitialContextFactory
Backend.JMS.ProviderURL=${Backend.ReleaseServiceUri}
Backend.JMS.QueueConnectionFactory=QueueConnectionFactory
RelConnector.SERVICEREQUEST_MUTATION.endpoint=custom-jms:queue:SERVICEREQUESTS?disableReplyTo=true
####ACTIVEMQ CONFIGURATIONS END########


#################################################################################################
### App Info - DO NOT add these properties in Remote Config
#################################################################################################
info.app.name = @project.parent.name@
info.app.description = @project.parent.description@
info.app.groupId = @project.parent.groupId@
info.app.artifactId = @project.parent.artifactId@
info.app.version = @project.version@
info.app.encoding = @project.build.sourceEncoding@
info.app.java.source = @java.version@
info.app.java.target = @java.version@

# this is used as sourceIdentity while publishing triggers/events
# The `application.identity` parameter holds preference, followed by CF App name as obtained from `VCAP_APPLICATION`, followed by `spring.application.name`.
application.identity=${spring.application.name}-${info.app.version}
#################################################################################################


#########ELASTICSEARCH CONFIGURATIONS STARTS######################
ES.Version=7
ES.Default.type=_doc
ES.DB.Protocol=http
ES.DB.Host=10.197.12.6
ES.DB.Port=9200
ES.DB.User=cbx-app
ES.DB.Password=s3cr3t
ES.DB.Conn.TimeoutMs=10000
ES.DB.Read.TimeoutMs=10000
ES.DB.SSL.DisableHostNameVerification=false
ES.Index.srq=quest.srq.servicerequests
ES.Query={"query":{"bool":{"must":{"bool":{"must":[{"terms":{"referenceNo.keyword":["##CH_SEQ_NUM##"]}}],"should":[],"must_not":[]}}}},"_source": ["stateInfo.initiated.ts","stateInfo.initiated.userInfo.userName","stateInfo.lastAction.userInfo.userName","stateInfo.lastAction.ts"]}
#########ELASTICSEARCH CONFIGURATIONS ENDS######################
